@def title = "Anamitra Sikdar"

![Panoroma!](/assets/images/panoroma.jpg "Mica schist from a heavily deformed terrain, Ghatsila, India")
![Anamitra Sikdar!](/assets/images/self.jpg "Ask sambit to take a good photo")
# About me
##### A postgraduate, currently employed as a PhD student in Indian Institute of Technology, Kanpur. Interested in shear instability growth in rocks. I integrate experimental and analytical data of natural rock samples to identify the deformation mechanisms. Possess a keen interest in scientific programming. Committed to continuous learning and professional development.
Official *[webpage](https://home.iitk.ac.in/~smisra/) of Experimental Rock Deformation Laboratory.