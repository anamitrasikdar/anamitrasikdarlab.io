+++
title = "Ongoing Research"
hascode = true
date = Date(2019, 3, 22)
rss = "A short description of the page which would serve as **blurb** in a `RSS` feed; you can use basic markdown here but the whole description string must be a single line (not a multiline string). Like this one for instance. Keep in mind that styling is minimal in RSS so for instance don't expect maths or fancy styling to work; images should be ok though: ![](https://upload.wikimedia.org/wikipedia/en/3/32/Rick_and_Morty_opening_credits.jpeg)"
+++
@def tags = ["syntax", "code"]
# Ongoing Research

\toc

## Overview

> The presence of shear instabilities within a stable Archean landmass is an intriguing phenomenon. Researchers tried to understand the mechanisms of shear instability formation in different geological set up and stable cratons are the least likely places of occurrence. I chose the Bundelkhand Craton in central India as the area for studying these kinds of shear bands. In addition to that, the thicknesses of the shear bands vary in the area. However, the controlling is yet unknown and must be addressed from a first principle approach. 


Technological advancements and introduction of modern techniques such as Electron Back Scattered Diffraction (EBSD), Transmission Electron Microscopy (TEM) etc., unveiled several avenues of research, particularly the study of deformation kinematics in intra-grain level. Width variation study of shear bands in microscopic scale has not been concluded yet. 
Shear band forms when rock attains plastic deformation. Net strain is accommodated in the grain scale in the form of dislocation or slips. The standard practice to probe any grain-scale deformation feature, is to analyze the orientation data. Crystallographic orientation is the key to identify the slip systems and the sequence of system activation, are clues for identifying the physical parameters in play. And this method has been proven both, experimentally (deforming different natural or synthetic crystals and rocks) and analytically (studying the naturally deformed rocks). 

<!--
 [the docs](https://tlienart.github.io/franklindocs/code/index.html#more_on_paths) for more info. If you don't care about how things are structured in your `/assets/` folder, just use `./scriptname.jl`. If you want things to be grouped, use `./group/scriptname.jl`. For more involved uses, see the docs.
-->


## Materials & method

> For the study, I have selected the granitic gneiss of Bundelkhand Craton. A detailed map has been produced from the available open datasets of Geological Survey of India. The datasets contain detailed petrological and structural data of the entire Bundelkhand Craton which covers around 26,000 sq kms. Mylonitization through recrystallization has been confirmed in this step along with phase identification and grain size analysis using ImageJ.

> I collected oriented core samples from the field. The plane of maximum deformation is identified and samples for electron imaging and optical microscopy are prepared. Thin sections are studied extensively to examine the grain boundaries of phases of interest. To examine the intra-grain dislocations, detailed electron back scattered diffraction (EBSD) imaging has been carried out. X ray diffraction (XRD) techniques has been used to identify the presence of any selective phase within the shear band. The data from the XRD is analyzed to verify the results from energy dispersive spectra (EDS) and area analysis of EBSD. The orientation data has been processed by an open toolbox in MATLAB, called MTEX.

## Work done so far

### Initial goal of understanding the microstructures from the oriented mylonite samples:

> Field work has been done. Oriented samples are cored out. Detailed petrographic study has been completed with optical microscope. Evidence of grain boundary migration is observed. Grains of feldspar are showing bulging recrystallisation whereas the quartz grains might go up to grain boundary area reduction.

### Identifying the evolution of the mylonitization process from quantitative textural analysis: 

> The sections of maximum deformation intensities (XZ sections) are analyzed with a Field Emission Electron Microscope. The orientation data along with the Energy Dispersive Spectroscopic maps of regions of interest in each sample are collected. The regions of interest are identified from the panoramic scans using Back Scattered Electron images from a Tungsten Scanning Electron Microscope.

@@row
@@container
![BSE Image!](/assets/site.png) 
@@
~~~
<div style="clear: both"></div>
~~~
@@

@@row
@@container
 ![phase map!](/assets/SB3A.jpg)
@@
~~~
<div style="clear: both"></div>
~~~
@@

> The orientation data has been processed by an open toolbox in MATLAB, called MTEX.

@@row
@@container 
 ![misorientation!](/assets/SB4_Host_Feldspar.jpg)
@@
~~~
<div style="clear: both"></div>
~~~
@@

> The slip system activation is being studied at this point. Spatial Schmid factor analysis might point to the answers I am looking for. Low angle grain boundary misorientation suggests the temperature of deformation is 600° C. So, it is certain that the temperature is not a factor in the thickness control. The mineralogy of the shear bands is different. I suspect that there might be a fluid activity, but it will not be right to conclude at this point.

## Future plans

• Probe into slip system and phase activation and phase control on shear zone width.

• 3D strain analysis will be performed to quantify the thinning and extrusion.

• TEM for identifying the dislocations at grain scale.

• The role of fluid in the journey of weak proto mylonite to ultra-mylonite in BGGC is intended to discover. Fourier transform infrared spectroscopy will be carried out for this goal.

• Numerical modelling integrating the chemical kinetics along with the physical parameters.
