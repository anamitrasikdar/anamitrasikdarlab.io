@def title = "Anamitra Sikdar"
@def tags = ["syntax", "code"]


\tableofcontents <!-- you can use \toc as well --> 



@@container
![Anamitra Sikdar!](/assets/self.jpg)
@@

# About me

A postgraduate, currently employed as a PhD student in Indian Institute of Technology, Kanpur. Interested in shear instability growth in rocks. Want to integrate experimental and analytical data of natural rock samples to identify the deformation mechanisms. Possess a keen interest in scientific programming. Committed to continuous learning and professional development.
Affiliated with the [Experimental Rock Deformation Laboratory](https://home.iitk.ac.in/~smisra/).

# Educational Background

**Indian Institute of Technology Kanpur Kanpur, India**

PhD Candidate, Earth Sciences (2019 – Present)


**Jadavpur University Kolkata, India**

MSc, Applied Geology (2015 – 2017)


**Jadavpur University Kolkata, India**

BSc, Geological Sciences (2012 – 2015)


# PhD Courses 

* Mathematics for Earth Sciences 

* Solid Earth Geophysics 

* Advanced Structural Geology 

* Rock Mechanics 

* Rock Deformation & Rock Physics 

* Instrumentation for Earth Sciences

# Gallery (Under the microscope)

@@row
@@container
![Panoroma!](/assets/panorama.png)
@@
Crossed Polar image of Mica schist from a heavily deformed terrain, Ghatsila, India
~~~
<div style="clear: both"></div>
~~~
@@